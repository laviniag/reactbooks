import React, { Component } from 'react';
import {Well, Form, FormGroup, ControlLabel, FormControl, Button} from 'react-bootstrap';

class SearchInput extends Component {
  constructor(props){
      super(props);
      this.state = {
        word: props.value
      }
  }

  handleChange(event){
      event.preventDefault();
      // console.log(event.target.value);
      let word = event.target.value;
      this.setState({word: word});
      if(word.length > 2) {
          this.props.onChange(word);
      }
  }

  render() {
    return (
      <Well>
          <Form inline>
              <FormGroup controlId="formInlineName">
                  <ControlLabel>Books</ControlLabel>
                  {' '}
                  <FormControl type="text" value={this.state.word}
                      placeholder="Search Books..." onChange={this.handleChange.bind(this)} />
              </FormGroup>
              {' '}
              <Button type="submit" onClick={this.handleChange.bind(this)}>Reset</Button>
          </Form>
      </Well>
    );
  }
}

export default SearchInput;
